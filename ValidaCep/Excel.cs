﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ValidaCep
{
    class Excel
    {
        string path = "";
        string valor = "";
        _Application excel = new _Excel.Application();
        Workbook workbook;
        Worksheet worksheet;
        int ultimaLinhaUsada = 0;

        public Excel()
        {

        }

        public Excel(string path, int Sheet)
        {
            try
            {
                this.path = path;
                this.workbook = excel.Workbooks.Open(path);
                this.worksheet = workbook.Worksheets[Sheet];
            }
            catch (Exception ex)
            {
                Console.WriteLine("O arquivo 'Lista_de_CEPs' nao foi encontrado no diretório 'Documentos'.");
                Console.WriteLine(ex);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        public void CriaNovoArquivo()
        {
            this.workbook = this.excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            this.worksheet = this.workbook.Worksheets[1];
            this.EscreverNaCelula(1, 1, "Logradouro/Nome");
            this.EscreverNaCelula(1, 2, "Bairro/Distrito");
            this.EscreverNaCelula(1, 3, "Localidade/UF");
            this.EscreverNaCelula(1, 4, "CEP");
            this.EscreverNaCelula(1, 5, "Data/Hora");
            this.SalvarComo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\resultado.xlsx");
        }

        public int ContarLinhasPreenchidas()
        {
            return ultimaLinhaUsada = this.worksheet.UsedRange.Rows.Count;
        }

        public string LerCelula(int linha, int coluna)
        {
            if (this.worksheet.Cells[linha, coluna].Value2 != null)
            {
                this.valor = Convert.ToString(this.worksheet.Cells[linha, coluna].Value2);
                return this.valor;
            }

            return "";
        }

        /**
         * Lê o conteúdo de uma planilha dentro de um intervalo de celulas e colunas
         * comecoI = Primeira Linha
         * comecoY = Primeira Coluna
         * fimI = Ultima Linha
         * fimY = Ultima Coluna
         */
        public string[,] LerIntervalo(int comecoI, int comecoY, int fimI, int fimY)
        {
            Range intervalo = (Range)worksheet.Range[worksheet.Cells[comecoI, comecoY], worksheet.Cells[fimI, fimY]];

            // Dentro do "Value2" estarão todos os dados do intervalo
            object[,] valores = intervalo.Value2;
            string[,] returnString = new string[fimI - comecoI, fimY - comecoY];
            for (int p = 1; p <= fimI - comecoI; p++)
            {
                for (int q = 1; q <= fimY - comecoY; q++)
                {
                    //returnString[p - 1, q - 1] = valores[p, q].ToString();
                    returnString[p - 1, q - 1] = valores[p, q].ToString();
                }
            }
            return returnString;
        }

        public void EscreverNaCelula(int linha, int coluna, string valor)
        {
            this.worksheet.Cells[linha, coluna].Value2 = valor;
        }

        public void Salvar()
        {
            this.workbook.Save();
        }

        public void SalvarComo(string caminho)
        {
            this.workbook.SaveAs(caminho);
        }

        public void Fechar(bool salvar = false)
        {
            if (salvar)
            {
                this.Salvar();
            }
            this.excel.Quit();
        }
    }
}
