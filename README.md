# ConsultaCEP

Desafio técnico cujo objetivo é o desenvolvimento de um Robô em C# com Selenium que consulta CEPs de uma dada planilha e, ao encontrar informações do CEP consultado, grava essas informações em uma nova planilha.

**Pré-Requisitos para Execução**
- Sistema Operacional Windows
- Visual Studio;
- Google Chrome;
- Microsoft Excel;
- Planilha chamada Lista_de_CEPs, no mesmo formato que o fornecido para o desafio técnico, que esteja localizada na pasta Documentos;

**Termo de Entrega**

https://docs.google.com/document/d/1QeEC0wBwWpzvMnwKzKzZdVAqtesqZPngcFEq4yLc3zk/edit?usp=sharing