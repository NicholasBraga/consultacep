﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace ValidaCep
{
    class Helper
    {
        /**
         * Calcula a quantidade de buscas por unidade de tempo
         */
        public String BuscasPorSegundo(double buscas, double tempoExecucao)
        {
            return string.Format("{0:F2}", (buscas / tempoExecucao));
        }

        /**
         * Contém o texto a ser exibido no final da execução do robô
         */
        public String ResumoExecucao(double cepsPesquisados, TimeSpan tempoDecorrido)
        {
            return $"\nProcessamento concluído: {cepsPesquisados} CEPs pesquisados em {tempoDecorrido.Hours} hora(s) {tempoDecorrido.Minutes} minuto(s) {tempoDecorrido.Seconds} segundo(s)." +
                $"\nMédia de { this.BuscasPorSegundo(cepsPesquisados, tempoDecorrido.TotalSeconds) } busca(s) por segundo." +
                $"\nPressione ENTER para encerrar...";
        }
    }
}
