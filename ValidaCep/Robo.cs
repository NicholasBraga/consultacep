﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Diagnostics;

namespace ValidaCep
{
    class Robo
    {
        Helper helper = new Helper();

        // Cria o objeto usado para medir o tempo de processamento
        Stopwatch stopwatch = new Stopwatch();

        // Cria uma instância do driver do ChromeDriver
        IWebDriver driver = new ChromeDriver();

        // Instancia o objeto passando para o construtor o arquivo a ser lido como primeiro parâmetro e o worksheet como segundo.
        // O caminho do arquivo será a pasta "Documentos"
        Excel excel = new Excel(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Lista_de_CEPs", 1);

        // Iterador de referência para saber onde os dados começarão a ser escritos
        int linhaInicio = 2;

        // Iterador de referência para saber quantos CEPs foram pesquisados
        int cepsPesquisados = 0;

        public void ExecutaRobo()
        {
            // Lê os dados da planilha começando pela linha 2 da segunda coluna e terminando na última linha preenchida da terceira coluna
            // e monta um array bidimensional onde [0,0] representa o valor da linha 2, coluna 2 e [0,1] representa o valor da linha 2, coluna 3
            string[,] ceps = this.excel.LerIntervalo(2, 2, this.excel.ContarLinhasPreenchidas(), 4);
            this.excel.Fechar();

            Console.WriteLine("\n\nCriando arquivo de saída...");

            this.excel.CriaNovoArquivo();

            Console.WriteLine("Arquivo de saída criado com sucesso!");
            Console.WriteLine("Iniciando busca de CEP...");

            // Inicia a contagem de tempo do processamento dos CEPs
            this.stopwatch.Start();

            // O laço externo percorre cada uma das faixas de CEP
            for (int i = 0; i <= ceps.Length; i++)
            {
                this.cepsPesquisados += Int32.Parse(ceps[i, 1]) - Int32.Parse(ceps[i, 0]);

                // O laço interno percorre o intervalo de CEP de cada faixa
                for (int j = Int32.Parse(ceps[i, 0].Substring(0, 5)); j <= Int32.Parse(ceps[i, 1].Substring(0, 5)); j++)
                {
                    String cep = j.ToString();

                    this.AcessaSite("http://www.buscacep.correios.com.br/sistemas/buscacep/BuscaCepEndereco.cfm");

                    try
                    {
                        Console.WriteLine($"Processando faixa de CEP {cep}");

                        this.ExecutaBusca(cep);

                        // Se o Selenium encontrar o elemento que representa o logradouro é sinal de que o CEP é válido, então, escreve na planilha as informações
                        if (this.driver.FindElement(By.XPath("//table[@class='tmptabela']")).Displayed)
                        {
                            this.EscreverNaPlanilha();
                        }
                    }
                    catch (Exception)
                    {
                        // Não para a execução
                    }
                }
            }

            this.stopwatch.Stop();

            TimeSpan ts = this.stopwatch.Elapsed;

            // Salva e fecha o excel
            this.excel.Fechar(true);

            // Encerra o driver do Chrome do Selenium
            this.driver.Close();

            Console.WriteLine(this.helper.ResumoExecucao(this.cepsPesquisados, ts));
            Console.ReadLine();
        }

        /**
         * Tenta acessar o site e verifica se o site está redirecionando para uma URL diferente da desejada (isso acontece quando identificam a ação do Robô). 
         * Caso esteja, fica num loop aguardando 1 minuto e tentando novamente.
         */
        private void AcessaSite(string url)
        {
            do
            {
                this.driver.Navigate().GoToUrl(url);
                if (this.driver.Url != url)
                {
                    Console.WriteLine($"Redirecionamento automático para {this.driver.Url}. Aguardando para nova tentativa...");
                    Thread.Sleep(60000);
                }
            } while (this.driver.Url != url);
        }

        private void ExecutaBusca(string cep)
        {
            // Recupera o campo de busca onde o CEP é digitado
            var inputBusca = this.driver.FindElement(By.Name("relaxation"));

            // Informa o CEP a ser buscado
            inputBusca.SendKeys(cep);

            // Recupera o botão que executa a busca por CEP
            var botaoBusca = this.driver.FindElement(By.ClassName("btn2"));

            // Clica no botão de busca
            botaoBusca.Click();
        }

        private void EscreverNaPlanilha()
        {
            bool continuar = true;
            do
            {
                int quantidadeResultados = this.driver.FindElements(By.XPath("//table[@class='tmptabela']//tbody//tr")).Count;

                // Começa a partir da segunda linha pois a primeira é sempre o cabeçalho
                for (int linha = 2; linha <= quantidadeResultados; linha++)
                {
                    for (int coluna = 1; coluna <= 5; coluna++)
                    {
                        if (coluna <= 4)
                        {
                            this.excel.EscreverNaCelula(this.linhaInicio, coluna, this.driver.FindElement(By.XPath($"//table[@class='tmptabela']//tbody//tr[{linha}]//td[{coluna}]")).Text.ToString());

                        }
                        else
                        {
                            this.excel.EscreverNaCelula(this.linhaInicio, coluna, DateTime.Now.ToString("G"));
                        }
                    }

                    this.linhaInicio++;

                }

                this.excel.Salvar();

                // Se existir o botão de "Próxima" (para consultar resultados na próxima página), clica nele, senão, passa para a outra faixa
                if (this.driver.FindElement(By.XPath("//a[@href=\"javascript:document.Proxima.submit('Proxima')\"]")).Displayed)
                {
                    this.driver.FindElement(By.XPath("//a[@href=\"javascript:document.Proxima.submit('Proxima')\"]")).Click();
                }
                else
                {
                    continuar = false;
                }

            } while (continuar);
        }
    }
}
